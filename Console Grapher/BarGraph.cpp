#include "BarGraph.h"
#include <algorithm>
#include <cmath>
#include <fstream>
#include "rapidjson-master/include/rapidjson/stringbuffer.h"
#include "rapidjson-master/include/rapidjson/writer.h"

using namespace std;

BarGraph::BarGraph(string file) : Graph(file) {
	if (good) {
		ParseDocument();
	}
}

void BarGraph::ParseDocument() {
	Graph::ParseDocument();
	maxYValue = 0.0;
	if (document.HasMember("displaySorted") && document["displaySorted"].IsNumber()) {
		displaySorted = document["displaySorted"].GetInt();
	}
	else {
		displaySorted = 0;
	}

	size_t longestBarLabel = 0;
	// Parse data array
	if (document.HasMember("data")) {
		// Using a reference for consecutive access is handy and faster.
		const rapidjson::Value &value = document["data"];
		if (value.IsArray()) {
			// Iterate through all bars and extract the x and y data
			// elements
			for (rapidjson::SizeType i = 0; i < value.Size(); ++i) {
				const rapidjson::Value &dataElem =
					value[i]; // ref for convenience
				// data element must have only 2 elements
				if (dataElem.IsArray() && dataElem.Size() == 2) {
					if (dataElem[0].IsString() && dataElem[1].IsNumber()) {
						string x = dataElem[0].GetString();
						double y = dataElem[1].GetDouble();
						// Negative bar heights are not supported.
						if (y >= 0.0) {
							data.emplace_back(x, y);
							maxYValue = max(maxYValue, y);
							longestBarLabel = max(longestBarLabel, x.length());
						}
					}
				}
			}
		}
	}
	numBars = data.size();
	if (displaySorted > 0) {
		// sort data by y value ascending
		std::sort(data.begin(), data.end(),
				  [](const Bar& a, const Bar& b) -> bool { return a.y < b.y; });
	}
	else if (displaySorted < 0) {
		// sort data by y value descending
		std::sort(data.begin(), data.end(),
				 [](const Bar& a, const Bar& b) -> bool { return a.y > b.y; });
	}
	if (!verticalX) {
		if (maxYValue <= 0) {
			yAxisTickWidth = 1;  // to prevent log10f throwing a domain error
		}
		else {
			yAxisTickWidth = (int)log10f(maxYValue) + 1;
		}
	}
	else {
		yAxisTickWidth = (int)longestBarLabel;
	}

    dataAreaLeft = yAxisLabelWidth + yAxisTickWidth + 2;
    dataAreaRight = WIDTH - 1;
    dataAreaTop = TITLE_HEIGHT + 1;
    dataAreaBottom = HEIGHT - 1 - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT;
    dataAreaWidth = dataAreaRight - dataAreaLeft + 1;
    dataAreaHeight = dataAreaBottom - dataAreaTop + 1;
}

void BarGraph::DrawAxes() {
	//_______________________________________________________
	// X Axis
	int xAxisStartCol = yAxisLabelWidth + yAxisTickWidth + 2;
	int xAxisWidth = WIDTH - xAxisStartCol;
	// Draw axis line
	for (int c = xAxisStartCol; c < WIDTH; ++c) {
		bitmap[toindex(HEIGHT - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT - 1,
					   c)] = '_';
	}
	// Draw label
	if (!verticalX) {
		DrawStringCentered(xAxis, HEIGHT - 1, xAxisStartCol + xAxisWidth / 2);
	}
	else {
		DrawStringCentered(yAxis, HEIGHT - 1, xAxisStartCol + xAxisWidth / 2);
	}

	//_______________________________________________________
	// Y Axis
	int graphHeight =
		HEIGHT - TITLE_HEIGHT - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT;
	int axisCenter = graphHeight / 2 + TITLE_HEIGHT;
	int yAxisCol = yAxisLabelWidth + yAxisTickWidth + 1;
	int yAxisEndRow = HEIGHT - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT;
	// Draw axis line
	for (int r = 1; r < yAxisEndRow; ++r) {
		bitmap[toindex(r, yAxisCol)] = '|';
	}
	// Draw label
	if (!verticalX) {
		DrawString(yAxis, axisCenter, 0);
	}
	else {
		DrawString(xAxis, axisCenter, 0);
	}
}

void BarGraph::DrawData() {
    if (numBars == 0) {
        DrawStringCentered("ERROR: No graph data.",
                           dataAreaTop + dataAreaHeight / 2,
                           dataAreaLeft + dataAreaWidth / 2);
        return;
    }

	if (!verticalX) {
		int sectionWidth = dataAreaWidth / numBars;
		int barWidth = sectionWidth / 2 + 1;

		// Draw bars and x axis ticks
		int col =
			dataAreaLeft + sectionWidth / 2; // draw bar centered at this column
		for (auto i = 0; i != numBars; ++i) {
			int barHeight = max(0.0, (data[i].y / maxYValue * dataAreaHeight));
			DrawVerticalBar(dataAreaBottom, col, barWidth, barHeight);
			DrawStringCentered(data[i].x, dataAreaBottom + 1, col);
			col += sectionWidth;
		}

		// Draw y axis ticks
		double yAxisScale = maxYValue / dataAreaHeight;
		for (int row = dataAreaBottom; row >= dataAreaTop;
		row -= VERTICAL_AXIS_TICK_INTERVAL) {
			int tick = (yAxisScale * (dataAreaBottom - row));
			DrawStringRightAligned(to_string(tick), row + 1, dataAreaLeft - 2);
		}
	}
	else {  // Vertical X Axis
		int sectionWidth = dataAreaHeight / numBars;
		int barWidth = sectionWidth / 2 + 1;

		// Draw bars and vertical x-axis ticks
		int row =
			dataAreaTop + sectionWidth / 2; // draw bar centered at this row
		for (auto i = 0; i != numBars; ++i) {
			int barHeight = max(0.0, (data[i].y / maxYValue * dataAreaWidth));
			DrawHorizontalBar(row, dataAreaLeft, barWidth, barHeight);
			DrawStringRightAligned(data[i].x, row, dataAreaLeft - 2);
			row += sectionWidth;
		}

		// Draw horizontal y-axis ticks
		double xAxisScale = maxYValue / dataAreaWidth;
		for (int col = dataAreaLeft; col <= dataAreaRight;
		col += VERTICAL_AXIS_TICK_INTERVAL) {
			int tick = (xAxisScale * (col - dataAreaLeft));
			DrawStringCentered(to_string(tick), dataAreaBottom + 1, col - 1);
		}
	}
}

// Draw a bar centered on col, sitting on row
void BarGraph::DrawVerticalBar(const int row, const int col, const int width, const int height) {
    string barTop(max(0, width - 2), BAR_TOP);
    string barBodyFill(max(0, width - 2), BAR_FILL);
    string barBody = BAR_SIDE + barBodyFill + BAR_SIDE;
    // Draw sides
    for (int i = 0; i < height - 1; ++i) {
        DrawStringCentered(barBody, row - i, col);
    }
    // Draw top
    DrawStringCentered(barTop, row - (height - 1), col);
}

// Draw a bar centered on row, with left edge on col
void BarGraph::DrawHorizontalBar(const int row, const int col, const int width, const int height) {
	string barEdge(height, BAR_HORIZONTAL_EDGE);
	string barBodyFill(max(0, height-1), BAR_FILL);
	string barBody = barBodyFill + BAR_SIDE;
	int topRow = row - width/2;
	// Draw top edge
	DrawString(barEdge, topRow, col);
	// Draw body
	for (int i = 0; i < width - 1; ++i) {
		DrawString(barBody, topRow + 1 + i, col);
	}
	// Draw bottom edge
	DrawString(barEdge, topRow + width - 1, col);
}

// Write a JSON formatted bar graph file to the output file stream
void CreateBarGraphFile(ofstream &os, const string &title, const string &xAxis, const string &yAxis, int displaySorted, bool verticalX, const vector<Bar> &data) {
	rapidjson::StringBuffer s;
    rapidjson::Writer<rapidjson::StringBuffer> writer(s);
    
    writer.StartObject();
    writer.String("graphType");
    writer.String("BarGraph");
    writer.String("title");
    writer.String(title.c_str());
    writer.String("xAxis");
    writer.String(xAxis.c_str());
    writer.String("yAxis");
    writer.String(yAxis.c_str());
    writer.String("verticalX");
    writer.Bool(verticalX);
    writer.String("displaySorted");
    writer.Int(displaySorted);
    writer.String("data");
    writer.StartArray();
    for (const Bar &bar : data) {
    	writer.StartArray();
    	writer.String(bar.x.c_str());
    	writer.Double(bar.y);
    	writer.EndArray();
    }
    writer.EndArray();
    writer.EndObject();
	
	os << s.GetString() << endl;
}

