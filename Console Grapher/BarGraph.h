/*
Creates a bar graph from a JSON bar graph file and displays it
on the console. Negative bar heights are not supported. 
*/

#pragma once
#include "Graph.h"
#include <vector>
#include <string>
#include <fstream>

struct Bar
{
	Bar(std::string x, double y) : x(x), y(y) {}
	std::string x;
	double y;
};

class BarGraph : public Graph {
  public:
    BarGraph(std::string file);

  private:
    static const char BAR_SIDE = '|';
    static const char BAR_TOP = '_';
    static const char BAR_FILL = '%';
	static const char BAR_HORIZONTAL_EDGE = '-';

	void ParseDocument();

	void DrawAxes() override;
    void DrawData() override;

    // draw a bar centered on col, sitting on row
	void DrawVerticalBar(const int row, const int col, const int width, const int height);
	void DrawHorizontalBar(const int row, const int col, const int width, const int height);

    //_____________________________________________________________
    // Data members

    std::vector<Bar> data;
    int numBars;
	int displaySorted; // +ve: ascending -ve: descending
	                   // 0: do not sort
};

void CreateBarGraphFile(std::ofstream &os, const std::string &title, const std::string &xAxis, const std::string &yAxis, int displaySorted, bool verticalX, const std::vector<Bar> &data);