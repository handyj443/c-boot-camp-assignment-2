#include "Graph.h"
#include <fstream>
#include <string>
#include <iostream>

using namespace std;

Graph::Graph() {
	yAxisTickWidth = 0;
	ClearBitmap();
}

Graph::Graph(string filepath) : Graph() {
	good = ReadDataFromFile(filepath);
}

Graph::~Graph() {}

bool Graph::ReadDataFromFile(string filepath) {
	ifstream in(filepath);
	// Read whole file into string - shamelessly copied from
	// http://tinyurl.com/q6ncekk
	string contents((std::istreambuf_iterator<char>(in)),
					std::istreambuf_iterator<char>());
	if (document.Parse(contents.c_str()).HasParseError()) {
		return false;
	}
	return true;
}

void Graph::ParseDocument() {
	if (document.HasMember("title") && document["title"].IsString()) {
		SetTitle(document["title"].GetString());
	}
	else {
		SetTitle("No title");
	}

	if (document.HasMember("verticalX") && document["verticalX"].IsBool()) {
		verticalX = document["verticalX"].GetBool();
	}
	else {
		verticalX = false;
	}

	if (document.HasMember("xAxis") && document["xAxis"].IsString()) {
		SetXAxisLabel(document["xAxis"].GetString());
	}
	else {
		SetXAxisLabel("No label");
	}

	if (document.HasMember("yAxis") && document["yAxis"].IsString()) {
		SetYAxisLabel(document["yAxis"].GetString());
	}
	else {
		SetYAxisLabel("No label");
	}

	if (!verticalX) {
		yAxisLabelWidth = yAxis.length();
	}
	else {
		yAxisLabelWidth = xAxis.length();
	}
}

void Graph::DrawString(const string &text, int row, int col) {
    for (int i = 0; i < (int)text.length(); ++i) {
        if (col + i == WIDTH) {
            break;
        }
        bitmap[toindex(row, col) + i] = text[i];
    }
}

void Graph::DrawStringCentered(const string &text, int row, int col) {
    col -= text.length() / 2;
    if (col < 0) {
        col = 0;
    }
    DrawString(text, row, col);
}

void Graph::DrawStringRightAligned(const string &text, int row, int col) {
    col -= text.length() - 1;
    if (col < 0) {
        col = 0;
    }
    DrawString(text, row, col);
}

void Graph::ClearBitmap() {
    for (int i = 0; i < WIDTH * HEIGHT; ++i) {
        bitmap[i] = ' ';
    }
}

void Graph::RenderBitmap(ostream &os) {
	// row 0
	for (int col = 0; col < WIDTH; ++col) {
		cout << bitmap[toindex(0, col)];
	}
	// subsequent rows
    for (int row = 1; row < HEIGHT; ++row) {
        cout << '\n';
        for (int col = 0; col < WIDTH; ++col) {
            cout << bitmap[toindex(row, col)];
        }
    }
	cout << endl;
}

void Graph::DrawTitle() { 
	DrawStringCentered(title, 0, WIDTH / 2); 
}

ostream& operator<<(ostream& os, Graph& graph) {
	if (graph.valid()) {
		graph.DrawTitle();
		graph.DrawAxes();
		graph.DrawData();
		graph.RenderBitmap(os);
	}
	else {
		os << "Oh no! Graph file was invalid.\n";
	}
	return os;
}

GraphType GetGraphType(string filepath) {
	ifstream in(filepath);
	// Read whole file into string - shamelessly copied from
	// http://tinyurl.com/q6ncekk
	string contents((std::istreambuf_iterator<char>(in)),
					std::istreambuf_iterator<char>());
	rapidjson::Document document;
	if (document.Parse(contents.c_str()).HasParseError()) {
		return GraphType::INVALID_GRAPH;
	}

	if (document.HasMember("graphType") && document["graphType"].IsString()) {
		string type = document["graphType"].GetString();
		if (type == "BarGraph") {
			return GraphType::BAR_GRAPH;
		}
		if (type == "Histogram") {
			return GraphType::HISTOGRAM;
		}
		if (type == "LineGraph") {
			return GraphType::LINE_GRAPH;
		}
		if (type == "PointGraph") {
			return GraphType::POINT_GRAPH;
		}
	}

	return GraphType::INVALID_GRAPH;
}