/* 
This class is the parent class for more specific graph types. 
The following derived classes have been implemented:
	-Point Graph
	-Bar Graph
	-Histogram
	-Line Graph
*/
 
#pragma once
#include <iostream>
#include <string>
#include "rapidjson-master/include/rapidjson/document.h"


enum class GraphType { INVALID_GRAPH, BAR_GRAPH, HISTOGRAM, 
	                   LINE_GRAPH, POINT_GRAPH, GRAPH_TYPES };

class Graph {
  public:
	Graph();
    Graph(std::string file);
    virtual ~Graph();

	// Render graph to output stream
	friend std::ostream& operator<<(std::ostream& os, Graph& graph);

	// Not actually needed:
    //std::string GetTitle() const { return title; }
    //std::string GetXAxisLabel() const { return xAxis; }
    //std::string GetYAxisLabel() const { return yAxis; }

    bool valid() { return good; }

  protected:
    static const int WIDTH = 79;
    static const int HEIGHT = 20;
    static const int TITLE_HEIGHT = 1;
    static const int X_AXIS_LABEL_HEIGHT = 1;
    static const int X_AXIS_TICK_HEIGHT = 1;
    static const int VERTICAL_AXIS_TICK_INTERVAL = 5;
    //_____________________________________________________________
    // Protected methods
    bool ReadDataFromFile(std::string file);
	virtual void ParseDocument();

	virtual void DrawAxes() = 0;
	virtual void DrawData() = 0;
	// Draw graph to bitmap
	void RenderBitmap(std::ostream &os);

    // Draw a string starting at row and col. Text going beyond the edge is
    // truncated.
    void DrawString(const std::string &text, int row, int col);

    // Draw a string centered at row/col.
    // This function does not check for invalid row/col!
    void DrawStringCentered(const std::string &text, int row, int col);

    // Draw a string aligned on the right at row/col.
    // This function does not check for invalid row/col!
    void DrawStringRightAligned(const std::string &text, int row, int col);

    static int inline toindex(int r, int c) { return r * WIDTH + c; }

	void SetTitle(const std::string &t) { title = t; };
	void SetXAxisLabel(const std::string &label) { xAxis = label; }
	void SetYAxisLabel(const std::string &label) {
		yAxis = label;
	}

	rapidjson::Document document;
    // Character array to display on console
    char bitmap[HEIGHT * WIDTH];
    std::string xAxis; // always one line high
    std::string yAxis; // variable width
    int yAxisLabelWidth;
    int yAxisTickWidth;
    int dataAreaLeft;
    int dataAreaRight;
    int dataAreaTop;
    int dataAreaBottom;
    int dataAreaWidth;
    int dataAreaHeight;
    double maxYValue;
	bool verticalX;
	bool good;
    //_____________________________________________________________
    // Private methods
  private:
    void ClearBitmap();
    void DrawTitle();

    std::string title; // always one line high
};

GraphType GetGraphType(std::string filename);