#include "Histogram.h"
#include <string>
#include <algorithm>
#include <cmath>

using namespace std;

Histogram::Histogram(string file) : Graph(file) {
	if (good) {
		ParseDocument();
	}
}

void Histogram::ParseDocument() {
	// Parse data array
	Graph::ParseDocument();
	maxXValue = 0.0;
	maxYValue = 0.0;
	if (document.HasMember("data")) {
		// Using a reference for consecutive access is handy and faster.
		const rapidjson::Value &value = document["data"];
		if (value.IsArray()) {
			// Iterate through all bars and extract the x and y data
			// elements
			for (rapidjson::SizeType i = 0; i < value.Size(); ++i) {
				const rapidjson::Value &dataElem =
					value[i]; // ref for convenience
				// data element must have only 2 elements
				if (dataElem.IsArray() && dataElem.Size() == 2) {
					if (dataElem[0].IsNumber() && dataElem[1].IsNumber()) {
						double x = dataElem[0].GetDouble();
						double y = dataElem[1].GetDouble();
						// Negative x and y values are not supported.
						if (x >= 0.0 && y >= 0.0) {
							data.emplace_back(x, y);
							maxXValue = max(maxXValue, x);
							maxYValue = max(maxYValue, y);
						}
					}
				}
			}
		}
	}
	numBars = data.size();

	// Ensure data is sorted by x
	std::sort(data.begin(), data.end(), 
			  [](const Point& a, const Point& b) -> bool { return a.x < b.x; });

	yAxisTickWidth = (int)log10f(maxYValue) + 1;
	dataAreaLeft = yAxisLabelWidth + yAxisTickWidth + 2;
	dataAreaRight = WIDTH - 1;
	dataAreaTop = TITLE_HEIGHT + 1;
	dataAreaBottom = HEIGHT - 1 - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT;
	dataAreaWidth = dataAreaRight - dataAreaLeft + 1;
	dataAreaHeight = dataAreaBottom - dataAreaTop + 1;
}

void Histogram::DrawAxes() {
	//_______________________________________________________
	// X Axis
	int xAxisStartCol = yAxisLabelWidth + yAxisTickWidth + 2;
	int xAxisWidth = WIDTH - xAxisStartCol;
	// Draw axis line
	for (int c = xAxisStartCol; c < WIDTH; ++c) {
		bitmap[toindex(HEIGHT - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT - 1,
					   c)] = '_';
	}
	// Draw label
	DrawStringCentered(xAxis, HEIGHT - 1, xAxisStartCol + xAxisWidth / 2);

	//_______________________________________________________
	// Y Axis
	int graphHeight =
		HEIGHT - TITLE_HEIGHT - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT;
	int axisCenter = graphHeight / 2 + TITLE_HEIGHT;
	int yAxisCol = yAxisLabelWidth + yAxisTickWidth + 1;
	int yAxisEndRow = HEIGHT - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT;
	// Draw axis line
	for (int r = 1; r < yAxisEndRow; ++r) {
		bitmap[toindex(r, yAxisCol)] = '|';
	}
	// Draw label
	DrawString(yAxis, axisCenter, 0);
}

void Histogram::DrawData() {
    if (numBars == 0) {
        DrawStringCentered("ERROR: No graph data.",
                           dataAreaTop + dataAreaHeight / 2,
                           dataAreaLeft + dataAreaWidth / 2);
        return;
    }

    double xAxisScale = maxXValue / dataAreaWidth;
    double yAxisScale = maxYValue / dataAreaHeight;

	// The bar drawing algorithm takes a point sample of y at each value of x
	// by linearly interpolating the two y values on either side.
	vector<Point>::size_type dataIndex = 0;
	for (int col = 0; col < dataAreaWidth; ++col) {
		double x = col * xAxisScale;
		double y;
		// Find first data[].x which is greater than x
		while (data[dataIndex].x < x) {
			++dataIndex;
		}
		// If a y value is found at precisely x, uses that y value. 
		if (data[dataIndex].x == x) {
			y = data[dataIndex].y;
		}
		// If there is no previous point to interpolate with, 
		// just use the first one.
		else if (dataIndex == 0) {
			y = data[dataIndex].y;
		}
		// Otherwise perform the interpolation
		else {
			y = lerp(x, data[dataIndex-1].x, data[dataIndex].x,
							data[dataIndex-1].y, data[dataIndex].y);
		}
		int height = round(y / maxYValue * dataAreaHeight);
		DrawBar(height, col + dataAreaLeft);
	}

    // Draw x axis ticks
    for (int col = dataAreaLeft; col <= dataAreaRight;
         col += HORIZONTAL_AXIS_TICK_INTERVAL) {
		int tick = (xAxisScale * (col - dataAreaLeft));
		DrawStringCentered(to_string(tick), dataAreaBottom + 1, col);
    }

    // Draw y axis ticks
    for (int row = dataAreaBottom; row >= dataAreaTop;
         row -= VERTICAL_AXIS_TICK_INTERVAL) {
        int tick = (yAxisScale * (dataAreaBottom - row));
        DrawStringRightAligned(to_string(tick), row + 1, dataAreaLeft - 2);
    }
}

// draw a vertical bar on col
void Histogram::DrawBar(const int height, const int col) {
	if (height <= dataAreaHeight) {
		for (int i = 0; i < height; ++i) {
			bitmap[toindex(dataAreaBottom - i, col)] = HISTOGRAM_FILL;
		}
	}
}