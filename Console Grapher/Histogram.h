/*
Creates a histogram from a JSON histogram file and displays it
on the console. Negative x and y values are not supported.
*/

#pragma once
#include "Graph.h"
#include <vector>
#include <string>

class Histogram : public Graph
{
public:
	Histogram(std::string file);

private:
	static const char HISTOGRAM_FILL = '#';

	void ParseDocument();

	void DrawAxes() override;
	void DrawData() override;

	// draw a histogram bar
	void DrawBar(int height, int col);

	// get linearly interpolated value of y in [y1,y2], given x in [x1,x2]
	inline double lerp(double x, double x1, double x2, double y1, double y2) {
		return y1 + (x - x1) * (y2 - y1)/(x2 - x1);
	}

	//_____________________________________________________________
	// Data members
    static const int HORIZONTAL_AXIS_TICK_INTERVAL = 10;
	
	struct Point
	{
		Point(double x, double y) : x(x), y(y) {}
		double x;
		double y;
	};
	std::vector<Point> data;
	int numBars;
	double maxXValue;
};

