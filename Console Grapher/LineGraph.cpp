#include "LineGraph.h"
#include <algorithm>
#include <cmath>
#include <fstream>
#include "rapidjson-master/include/rapidjson/stringbuffer.h"
#include "rapidjson-master/include/rapidjson/writer.h"

using namespace std;

LineGraph::LineGraph(string file) : Graph(file) {
	if (good) {
		ParseDocument();
	}
}

void LineGraph::ParseDocument() {
	// Parse data array
	Graph::ParseDocument();
	maxXValue = 0.0;
	maxYValue = 0.0;
	if (document.HasMember("data")) {
		// Using a reference for consecutive access is handy and faster.
		const rapidjson::Value &value = document["data"];
		if (value.IsArray()) {
			// Iterate through all bars and extract the x and y data
			// elements
			for (rapidjson::SizeType i = 0; i < value.Size(); ++i) {
				const rapidjson::Value &dataElem =
					value[i]; // ref for convenience
				// data element must have only 2 elements
				if (dataElem.IsArray() && dataElem.Size() == 2) {
					if (dataElem[0].IsNumber() && dataElem[1].IsNumber()) {
						double x = dataElem[0].GetDouble();
						double y = dataElem[1].GetDouble();
						// Negative x and y values are not supported.
						if (x >= 0.0 && y >= 0.0) {
							data.emplace_back(x, y);
							maxXValue = max(maxXValue, x);
							maxYValue = max(maxYValue, y);
						}
					}
				}
			}
		}
	}
	numPoints = data.size();

	// Ensure data is sorted by x
	std::sort(data.begin(), data.end(),
			  [](const Point& a, const Point& b) -> bool { return a.x < b.x; });

	yAxisTickWidth = (int)log10f(maxYValue) + 1;
	dataAreaLeft = yAxisLabelWidth + yAxisTickWidth + 2;
	dataAreaRight = WIDTH - 1;
	dataAreaTop = TITLE_HEIGHT + 1;
	dataAreaBottom = HEIGHT - 1 - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT;
	dataAreaWidth = dataAreaRight - dataAreaLeft + 1;
	dataAreaHeight = dataAreaBottom - dataAreaTop + 1;
}

void LineGraph::DrawAxes() {
	//_______________________________________________________
	// X Axis
	int xAxisStartCol = yAxisLabelWidth + yAxisTickWidth + 2;
	int xAxisWidth = WIDTH - xAxisStartCol;
	// Draw axis line
	for (int c = xAxisStartCol; c < WIDTH; ++c) {
		bitmap[toindex(HEIGHT - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT - 1,
					   c)] = '_';
	}
	// Draw label
	DrawStringCentered(xAxis, HEIGHT - 1, xAxisStartCol + xAxisWidth / 2);

	//_______________________________________________________
	// Y Axis
	int graphHeight =
		HEIGHT - TITLE_HEIGHT - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT;
	int axisCenter = graphHeight / 2 + TITLE_HEIGHT;
	int yAxisCol = yAxisLabelWidth + yAxisTickWidth + 1;
	int yAxisEndRow = HEIGHT - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT;
	// Draw axis line
	for (int r = 1; r < yAxisEndRow; ++r) {
		bitmap[toindex(r, yAxisCol)] = '|';
	}
	// Draw label
	DrawString(yAxis, axisCenter, 0);
}

void LineGraph::DrawData() {
    if (numPoints == 0) {
        DrawStringCentered("ERROR: No graph data.",
                           dataAreaTop + dataAreaHeight / 2,
                           dataAreaLeft + dataAreaWidth / 2);
        return;
    }

	// Draw first point
	DrawDataPoint(data[0].x, data[0].y, POINT);

	int prevX = data[0].x;
	int prevY = data[0].y;
	// Draw the subsequent points and draw lines between them
	for (int i = 1; i < numPoints; ++i) {
		DrawLine(prevX, prevY, data[i].x, data[i].y);
		DrawDataPoint(data[i].x, data[i].y, POINT);
		prevX = data[i].x;
		prevY = data[i].y;
	}

    // Draw x axis ticks
	double xAxisScale = maxXValue / dataAreaWidth;
    for (int col = dataAreaLeft; col <= dataAreaRight;
         col += HORIZONTAL_AXIS_TICK_INTERVAL) {
		int tick = (xAxisScale * (col - dataAreaLeft));
		DrawStringCentered(to_string(tick), dataAreaBottom + 1, col - 1);
    }

    // Draw y axis ticks
	double yAxisScale = maxYValue / dataAreaHeight;
    for (int row = dataAreaBottom; row >= dataAreaTop;
         row -= VERTICAL_AXIS_TICK_INTERVAL) {
        int tick = (yAxisScale * (dataAreaBottom - row));
        DrawStringRightAligned(to_string(tick), row + 1, dataAreaLeft - 2);
    }
}

void LineGraph::DrawDataPoint(const double x, const double y, const char point) {
	double xScaled = x / maxXValue * dataAreaWidth;
	double yScaled = y / maxYValue * dataAreaHeight;
	int row = dataAreaBottom - yScaled + 1;
	int col = xScaled + dataAreaLeft - 1;
	assert(col >= dataAreaLeft - 1);
	assert(col <= dataAreaRight);
	assert(row <= dataAreaBottom + 1);
	assert(row >= dataAreaTop);
	// This ugly if statement is needed because values close to 0 are too 
	// small to show up on the graph due to the low resolution
	if (row <= dataAreaBottom && col >= dataAreaLeft) {
		bitmap[toindex(row, col)] = point;
	}
}

void LineGraph::DrawLine(const double x1, const double y1, const double x2, const double y2) {
	assert(x2 >= x1);
	double xAxisScale = maxXValue / dataAreaWidth;
	double yAxisScale = maxYValue / dataAreaHeight;

	// tan(22.5)
	const double HORIZONTAL_THRESHOLD_GRADIENT = 0.414 * yAxisScale / xAxisScale; 
	// tan(67.5)
	const double VERTICAL_THRESHOLD_GRADIENT = 2.414 * yAxisScale / xAxisScale;  

	double deltaX = x2 - x1;  // always positive
	double deltaY = y2 - y1;
	// Select correct line character to use depending on the gradient
	double m = deltaY / deltaX;
	char LINESEG;
	if (abs(m) < HORIZONTAL_THRESHOLD_GRADIENT) {
		LINESEG = LINE_HORIZONTAL;
	}
	else if (abs(m) > VERTICAL_THRESHOLD_GRADIENT) {
		LINESEG = LINE_VERTICAL;
	}
	else if (m > 0) {
		LINESEG = LINE_SLOPE_UP;
	}
	else {
		LINESEG = LINE_SLOPE_DOWN;
	}

	if (deltaX/xAxisScale > abs(deltaY)/yAxisScale) {
		//printf("New horizontal line:\n");
		// draw by X
		for (double x = x1 + xAxisScale; x < x2; x += xAxisScale) {
			double y = y1 + (x-x1)*m;
			//printf("x: %f, y: %f\n", x, y);
			DrawDataPoint(x, y, LINESEG);
		}
	}
	else {
		//printf("New vertical line:\n");
		m = 1 / m;
		if (y2 > y1) {
			// draw by Y (sloping up)
			for (double y = y1 + yAxisScale; y < y2; y += yAxisScale) {
				double x = x1 + (y-y1)*m;
				//printf("x: %f, y: %f\n", x, y);
				DrawDataPoint(x,y, LINESEG);
			}
		}
		else {
			// draw by Y (sloping down)
			for (double y = y1 - yAxisScale; y > y2; y -= yAxisScale) {
				double x = x1 - (y1-y)*m;
				//printf("x: %f, y: %f\n", x, y);
				DrawDataPoint(x, y, LINESEG);
			}
		}
	}
}

void CreateLineGraphFile(ofstream &os, const string &title, const string &xAxis, const string &yAxis, const vector<Point> &data) {
	rapidjson::StringBuffer s;
	rapidjson::Writer<rapidjson::StringBuffer> writer(s);

	writer.StartObject();
	writer.String("graphType");
	writer.String("LineGraph");
	writer.String("title");
	writer.String(title.c_str());
	writer.String("xAxis");
	writer.String(xAxis.c_str());
	writer.String("yAxis");
	writer.String(yAxis.c_str());
	writer.String("verticalX");
	writer.Bool(false);
	writer.String("data");
	writer.StartArray();
	for (const Point &point : data) {
		writer.StartArray();
		writer.Double(point.x);
		writer.Double(point.y);
		writer.EndArray();
	}
	writer.EndArray();
	writer.EndObject();

	os << s.GetString() << endl;
}