/*
Creates a histogram from a JSON histogram file and displays it
on the console. Negative x and y values are not supported.
*/

#pragma once
#include "Graph.h"
#include <vector>
#include <string>

#define SLOPES

struct Point
{
	Point(double x, double y) : x(x), y(y) {}
	double x;
	double y;
};

class LineGraph : public Graph
{
public:
	LineGraph(std::string file);

private:
	static const char POINT = 'X';
#ifdef SLOPES
	static const char LINE_HORIZONTAL = '-';
	static const char LINE_VERTICAL = '|';
	static const char LINE_SLOPE_UP = '/';
	static const char LINE_SLOPE_DOWN = '\\';
#else
	static const char LINE_HORIZONTAL = '.';
	static const char LINE_VERTICAL = '.';
	static const char LINE_SLOPE_UP = '.';
	static const char LINE_SLOPE_DOWN = '.';
#endif

	void ParseDocument();

	void DrawAxes() override;
	void DrawData() override;

	void DrawDataPoint(double x, double y, char point);
	void DrawLine(double x1, double y1, double x2, double y2);

	//_____________________________________________________________
	// Data members
    static const int HORIZONTAL_AXIS_TICK_INTERVAL = 10;

	std::vector<Point> data;
	int numPoints;
	double maxXValue;
};

void CreateLineGraphFile(std::ofstream &os, const std::string &title, const std::string &xAxis, const std::string &yAxis, const std::vector<Point> &data);