#include "PointGraph.h"
#include <string>
#include <algorithm>
#include <cmath>

using namespace std;

PointGraph::PointGraph(string file) : Graph(file) {
	if (good) {
		ParseDocument();
	}
}

void PointGraph::ParseDocument() {
	// Parse data array
	Graph::ParseDocument();
	maxXValue = 0.0;
	maxYValue = 0.0;
	if (document.HasMember("data")) {
		// Using a reference for consecutive access is handy and faster.
		const rapidjson::Value &value = document["data"];
		if (value.IsArray()) {
			// Iterate through all bars and extract the x and y data
			// elements
			for (rapidjson::SizeType i = 0; i < value.Size(); ++i) {
				const rapidjson::Value &dataElem =
					value[i]; // ref for convenience
				// data element must have only 2 elements
				if (dataElem.IsArray() && dataElem.Size() == 2) {
					if (dataElem[0].IsNumber() && dataElem[1].IsNumber()) {
						double x = dataElem[0].GetDouble();
						double y = dataElem[1].GetDouble();
						// Negative x and y values are not supported.
						if (x >= 0.0 && y >= 0.0) {
							data.emplace_back(x, y);
							maxXValue = max(maxXValue, x);
							maxYValue = max(maxYValue, y);
						}
					}
				}
			}
		}
	}
	numPoints = data.size();
	if (!verticalX) {
		if (maxYValue <= 0) {
			yAxisTickWidth = 1;  // to prevent log10f throwing a domain error
		}
		else {
			yAxisTickWidth = (int)log10f(maxYValue) + 1;
		}
	}
	else {
		if (maxXValue <= 0) {
			yAxisTickWidth = 1;  // to prevent log10f throwing a domain error
		}
		else {
			yAxisTickWidth = (int)log10f(maxXValue) + 1;
		}
	}

	yAxisTickWidth = (int)log10f(maxYValue) + 1;
	dataAreaLeft = yAxisLabelWidth + yAxisTickWidth + 2;
	dataAreaRight = WIDTH - 1;
	dataAreaTop = TITLE_HEIGHT + 1;
	dataAreaBottom = HEIGHT - 1 - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT;
	dataAreaWidth = dataAreaRight - dataAreaLeft + 1;
	dataAreaHeight = dataAreaBottom - dataAreaTop + 1;
}

void PointGraph::DrawAxes() {
	//_______________________________________________________
	// X Axis
	int xAxisStartCol = yAxisLabelWidth + yAxisTickWidth + 2;
	int xAxisWidth = WIDTH - xAxisStartCol;
	// Draw axis line
	for (int c = xAxisStartCol; c < WIDTH; ++c) {
		bitmap[toindex(HEIGHT - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT - 1,
					   c)] = '_';
	}
	// Draw label
	if (!verticalX) {
		DrawStringCentered(xAxis, HEIGHT - 1, xAxisStartCol + xAxisWidth / 2);
	}
	else {
		DrawStringCentered(yAxis, HEIGHT - 1, xAxisStartCol + xAxisWidth / 2);
	}

	//_______________________________________________________
	// Y Axis
	int graphHeight =
		HEIGHT - TITLE_HEIGHT - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT;
	int axisCenter = graphHeight / 2 + TITLE_HEIGHT;
	int yAxisCol = yAxisLabelWidth + yAxisTickWidth + 1;
	int yAxisEndRow = HEIGHT - X_AXIS_LABEL_HEIGHT - X_AXIS_TICK_HEIGHT;
	// Draw axis line
	for (int r = 1; r < yAxisEndRow; ++r) {
		bitmap[toindex(r, yAxisCol)] = '|';
	}
	// Draw label
	if (!verticalX) {
		DrawString(yAxis, axisCenter, 0);
	}
	else {
		DrawString(xAxis, axisCenter, 0);
	}
}

void PointGraph::DrawData() {
    if (numPoints == 0) {
        DrawStringCentered("ERROR: No graph data.",
                           dataAreaTop + dataAreaHeight / 2,
                           dataAreaLeft + dataAreaWidth / 2);
        return;
    }
	if (!verticalX) {
		// Draw each point
		for (const Point &p : data) {
			double x = p.x / maxXValue * dataAreaWidth;
			double y = p.y / maxYValue * dataAreaHeight;
			int col = x + dataAreaLeft - 1;

			int row = dataAreaBottom - y + 1;
			assert(col >= dataAreaLeft - 1);
			assert(col <= dataAreaRight);
			assert(row <= dataAreaBottom + 1);
			assert(row >= dataAreaTop);
			// This ugly if statement is needed because some values are too 
			// small to show up on the graph due to the low resolution
			if (row <= dataAreaBottom && col >= dataAreaLeft) {
				bitmap[toindex(row, col)] = POINT;
			}
		}

		// Draw x axis ticks
		double xAxisScale = maxXValue / dataAreaWidth;
		for (int col = dataAreaLeft; col <= dataAreaRight;
		col += HORIZONTAL_AXIS_TICK_INTERVAL) {
			int tick = (xAxisScale * (col - dataAreaLeft));
			DrawStringCentered(to_string(tick), dataAreaBottom + 1, col - 1);
		}

		// Draw y axis ticks
		double yAxisScale = maxYValue / dataAreaHeight;
		for (int row = dataAreaBottom; row >= dataAreaTop;
		row -= VERTICAL_AXIS_TICK_INTERVAL) {
			int tick = (yAxisScale * (dataAreaBottom - row));
			DrawStringRightAligned(to_string(tick), row + 1, dataAreaLeft - 2);
		}
	}
	else {  // Vertical X Axis
		// Draw each point
		for (const Point &p : data) {
			double x = p.x / maxXValue * dataAreaHeight;
			double y = p.y / maxYValue * dataAreaWidth;
			int row = dataAreaBottom - x + 1;
			int col = y + dataAreaLeft - 1;

			assert(col >= dataAreaLeft - 1);
			assert(col <= dataAreaRight);
			assert(row <= dataAreaBottom + 1);
			assert(row >= dataAreaTop);
			// This ugly if statement is needed because some values are too 
			// small to show up on the graph due to the low resolution
			if (row <= dataAreaBottom && col >= dataAreaLeft) {
				bitmap[toindex(row, col)] = POINT;
			}
		}

		// Draw vertical x-axis ticks
		double xAxisScale = maxXValue / dataAreaHeight;
		for (int row = dataAreaBottom; row >= dataAreaTop;
		row -= VERTICAL_AXIS_TICK_INTERVAL) {
			int tick = (xAxisScale * (dataAreaBottom - row));
			DrawStringRightAligned(to_string(tick), row + 1, dataAreaLeft - 2);
		}

		// Draw horizontal y-axis ticks
		double yAxisScale = maxYValue / dataAreaWidth;
		for (int col = dataAreaLeft; col <= dataAreaRight;
		col += HORIZONTAL_AXIS_TICK_INTERVAL) {
			int tick = (yAxisScale * (col - dataAreaLeft));
			DrawStringCentered(to_string(tick), dataAreaBottom + 1, col - 1);
		}
	}
}