/*
Creates a histogram from a JSON histogram file and displays it
on the console. Negative x and y values are not supported.
*/

#pragma once
#include "Graph.h"
#include <vector>
#include <string>

class PointGraph : public Graph
{
public:
	PointGraph(std::string file);

private:
	static const char POINT = 'X';

	void ParseDocument();

	void DrawAxes() override;
	void DrawData() override;

	//_____________________________________________________________
	// Data members
    static const int HORIZONTAL_AXIS_TICK_INTERVAL = 10;
	
	struct Point
	{
		Point(double x, double y) : x(x), y(y) {}
		double x;
		double y;
	};
	std::vector<Point> data;
	int numPoints;
	double maxXValue;
};

