/*
This program reads in a graph from a file and displays the graph on the console.
The user may also input data to create a new graph and save it to a file.
*/

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>
#include <cctype>
#include <cstdio>

#include "BarGraph.h"
#include "Histogram.h"
#include "PointGraph.h"
#include "LineGraph.h"
#include "rapidjson-master/include/rapidjson/prettywriter.h"
#include "rapidjson-master/include/rapidjson/filewritestream.h"
#include "rapidjson-master/include/rapidjson/filereadstream.h"

using namespace std;

//#define QUICKLOAD

const string graphFilePath = "graphs\\";

void DisplayGraphRoutine();
void CreateGraphRoutine();
void CreateBarGraph();
void CreateLineGraph();
void JSONPrettyPrint(string filename);

//___________________________________________________________________________
// PROGRAM START
int main() {

#ifdef QUICKLOAD
	BarGraph g("bargraph.json");
	cout << g;
	while (!_kbhit());
#else

	bool running = true;
	cout << "Welcome to the Muffin Labs graph manipulation utility.\n\n";

	while (running) {
		int option;
		string input;
		bool validInt;
		cout << "   MAIN MENU\t(1) Display a graph\n"
			"\t\t(2) Create a new graph\n"
			"\t\t(3) Quit\n";
		cout << "Enter option number: ";
		getline(cin, input);
		try {
			option = stoi(input);
			validInt = true;
		}
		catch (exception) {
			validInt = false;
		}
		if (validInt) {
			switch (option) {
				case 1:
					DisplayGraphRoutine();
					break;
				case 2:
					CreateGraphRoutine();
					break;
				case 3:
					running = false;
					break;
				default:
					cout << "ERROR: Invalid option.\n\n";
			}
		}
		else {
			cout << "ERROR: Invalid option.\n\n";
		}
	}
#endif
}

void DisplayGraphRoutine() {
	string filename;
	Graph *graph = nullptr;
	cout << "Enter graph file name to display: ";
	getline(cin, filename);
	string filepath = graphFilePath + filename;

	GraphType type = GetGraphType(filepath);
	switch (type) {
		case GraphType::BAR_GRAPH:
			graph = new BarGraph(filepath);
			break;

		case GraphType::HISTOGRAM:
			graph = new Histogram(filepath);
			break;

		case GraphType::LINE_GRAPH:
			graph = new LineGraph(filepath);
			break;

		case GraphType::POINT_GRAPH:
			graph = new PointGraph(filepath);
			break;
	}

	if (graph && graph->valid()) {
		cout << *graph << endl;
	}
	else {
		cout << "ERROR: File not found or invalid file format.\n\n";
	}
}

void CreateGraphRoutine() {
	int option;
	string input;
	bool validInt;
	while (true) {
		cout << "\n   CREATE GRAPH\t(1) Line Graph\n"
			"\t\t(2) Bar Graph\n"
			"\t\t(3) Back\n";
		cout << "Enter option number: ";
		getline(cin, input);
		try {
			option = stoi(input);
			validInt = true;
		}
		catch (exception) {
			validInt = false;
		}
		if (validInt) {
			switch (option) {
				case 1:
					CreateLineGraph();
					return;
				case 2:
					CreateBarGraph();
					return;
				case 3:
					cout << "\n";
					return;
				default:
					cout << "ERROR: Invalid option.\n";
			}
		}
		else {
			cout << "ERROR: Invalid option.\n";
		}
	}
}

// Creates a JSON bargraph file and displays it.
// NOTE: it would save a file read to create a BarGraph object first, then 
// write the file, but this way works well enough...
void CreateBarGraph() {
	string filename;
	cout << "Enter filename to create. "
		"(CAUTION: If the file already exists it will be overwritten): ";
	getline(cin, filename);
	string filepath = graphFilePath + filename;
	ofstream graphFile(filepath);;
	if (!graphFile) {
		cout << "ERROR: file could not be created.";
		return;
	}
	string title;
	cout << "Enter graph title: ";
	getline(cin, title);
	cin.clear();

	//document["title"] = title;
	string xAxis;
	cout << "Enter x-axis label: ";
	getline(cin, xAxis);
	cin.clear();

	string yAxis;
	cout << "Enter y-axis label: ";
	getline(cin, yAxis);
	cin.clear();

	string input;
	int displaySorted = 0;
	cout << "Should bars be displayed in (a)scending or (d)escending "
		"order? Leave blank for unsorted data: ";
	getline(cin, input);
	if (input.length() == 1) {
		if (input[0] == 'a' || input[0] == 'A') {
			displaySorted = 1;
		}
		else if (input[0] == 'd' || input[0] == 'D') {
			displaySorted = -1;
		}
	}
	cin.clear();

	bool verticalX = false;
	cout << "Should bars be (h)orizontal? Leave blank for vertical bars: ";
	getline(cin, input);
	if (input.length() == 1) {
		if (input[0] == 'h' || input[0] == 'H') {
			verticalX = true;
		}
	}
	cin.clear();

	vector<Bar> data;
	string dataInput;
	int numBars = 0;
	cout << "Enter the bar label and height separated by a space, one line at a time, followed by End Of File (Ctrl-Z): ";
	while (getline(cin, dataInput)) {
		const char *WHITESPACE = " \t";
		if (dataInput.size() == 0) {
			cout << "ERROR: Invalid input - nothing entered.\n";
			continue; // no input
		}

		// Extract the last continous sequence of non-whitespace characters
		size_t lastChar;
		if (isspace(dataInput.back())) {
			lastChar = dataInput.find_last_not_of(WHITESPACE);
		}
		else {
			lastChar = dataInput.size() - 1;
		}

		auto firstChar = dataInput.find_last_of(WHITESPACE, lastChar);
		if (firstChar == string::npos) {
			cout << "ERROR: Invalid input - must enter label and height.\n";
			continue;
		}
		++firstChar;
		size_t range = lastChar - firstChar + 1;
		double y = 0.0;
		try {
			y = stod(dataInput.substr(firstChar, range));
		}
		catch (exception) {
			cout << "ERROR: Invalid input - height must be a number.\n";
			continue;
		}

		// Extract the remaining sequence of non-whitespace characters
		lastChar = dataInput.find_last_not_of(WHITESPACE, firstChar - 1);
		firstChar = dataInput.find_first_not_of(WHITESPACE);
		range = lastChar - firstChar + 1;
		if (lastChar == string::npos || range <= 0) {
			cout << "ERROR: Invalid input - no label entered.\n";
			continue;
		}
		string x = dataInput.substr(firstChar, range);
		
		// Add the new bar (finally!)
		data.emplace_back(x, y);
		cout << "New bar added. Total number of bars: " << ++numBars << endl;
	}
	cin.clear();
	cout << "Bar graph created with " << numBars << " bars.\n";

	// Save the graph to file
	CreateBarGraphFile(graphFile, title, xAxis, yAxis, displaySorted, verticalX, data);
	graphFile.close();
	// Make the JSON file human-readable
	JSONPrettyPrint(filename);  
	// Display the newly created graph
	cout << BarGraph(filepath);
}

// Creates a JSON linegraph file and displays it.
void CreateLineGraph() {
	string filename;
	cout << "Enter filename to create. "
		"(CAUTION: If the file already exists it will be overwritten): ";
	getline(cin, filename);
	string filepath = graphFilePath + filename;
	ofstream graphFile(filepath);;
	if (!graphFile) {
		cout << "ERROR: file could not be created.";
		return;
	}
	string title;
	cout << "Enter graph title: ";
	getline(cin, title);
	cin.clear();

	string xAxis;
	cout << "Enter x-axis label: ";
	getline(cin, xAxis);
	cin.clear();

	string yAxis;
	cout << "Enter y-axis label: ";
	getline(cin, yAxis);
	cin.clear();

	vector<Point> data;
	string dataInput;
	int numPoints = 0;
	cout << "Enter the x and y values separated by a space, one line at a time, followed by End Of File (Ctrl-Z): ";
	while (getline(cin, dataInput)) {
		const char *WHITESPACE = " \t";

		// Extract the first number
		auto firstChar = dataInput.find_first_not_of(WHITESPACE);
		if (firstChar == string::npos) {
			cout << "ERROR: Invalid input - nothing entered.\n";
			continue; // no input
		}
		auto lastChar = dataInput.find_first_of(WHITESPACE, firstChar);
		if (lastChar == string::npos) {
			cout << "ERROR: Invalid input - no y value entered.\n";
			continue; 
		}
		auto strRange = lastChar - firstChar;
		double x = 0.0;
		try {
			x = stod(dataInput.substr(firstChar, strRange));
		}
		catch (exception) {
			cout << "ERROR: Invalid input - x value was not a number.\n";
			continue;
		}

		// Extract the second number
		firstChar = dataInput.find_first_not_of(WHITESPACE, lastChar);
		if (firstChar == string::npos) {
			cout << "ERROR: no y value entered.\n";
			continue; 
		}
		double y = 0.0;
		try {
			y = stod(dataInput.substr(firstChar));
		}
		catch (exception) {
			cout << "ERROR: y value was not a number.\n";
			continue;
		}

		// Add the new point
		data.emplace_back(x, y);
		cout << "New point added. Total number of points: " << ++numPoints << endl;
	}
	cin.clear();
	cout << "Line graph created with " << numPoints << " points.\n";

	// Save the graph to file
	CreateLineGraphFile(graphFile, title, xAxis, yAxis, data);
	graphFile.close();
	// Make the JSON file human-readable
	JSONPrettyPrint(filename);
	// Display the newly created graph
	cout << LineGraph(filepath);
}

// Reformat the JSON file to be human-readable
// Possibly the worst abuse of the rapidjson API that has ever been written
// but it works...
void JSONPrettyPrint(string filename) {
	// horrible write to a new file which is renamed at the end :((
	string readfilepath = graphFilePath + filename;
	string writefilepath = graphFilePath + filename + "_temp";
	// horrible arbritary size limit to buffer :(
	const size_t MAX_SIZE = 65536;
	FILE *fp;
	fopen_s(&fp, readfilepath.c_str(), "r");
	char readBuffer[MAX_SIZE];
	rapidjson::Reader reader;
	rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));

	FILE *fpw;
	fopen_s(&fpw, writefilepath.c_str(), "w");
	char writeBuffer[MAX_SIZE];
	rapidjson::FileWriteStream os(fpw, writeBuffer, sizeof(writeBuffer));

	rapidjson::PrettyWriter<rapidjson::FileWriteStream> prettyWriter(os);
	reader.Parse<rapidjson::kParseValidateEncodingFlag>(is, prettyWriter);
	fclose(fp);
	fclose(fpw);

	remove(readfilepath.c_str());
	rename(writefilepath.c_str(), readfilepath.c_str());
}