#include "rapidjson-master\include\rapidjson\document.h"
#include "rapidjson-master\include\rapidjson\prettywriter.h"
#include "rapidjson-master\include\rapidjson\stringbuffer.h"
#include "rapidjson-master\include\rapidjson\filereadstream.h"
#include "rapidjson-master\include\rapidjson\filewritestream.h"
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <conio.h>

using namespace std;

using namespace rapidjson;

int main(int, char*[]) {
	rapidjson::StringBuffer s;
	rapidjson::Writer<rapidjson::StringBuffer> writer(s);

	writer.StartObject();
	writer.String("hello");
	writer.String("world");
	writer.String("t");
	writer.Bool(true);
	writer.String("f");
	writer.Bool(false);
	writer.String("n");
	writer.Null();
	writer.String("i");
	writer.Uint(123);
	writer.String("pi");
	writer.Double(3.1416);
	writer.String("a");
	writer.StartArray();
	for (unsigned i = 0; i < 4; i++)
		writer.Uint(i);
	writer.EndArray();
	writer.EndObject();

	// Super hacky string massaging
	string temp = s.GetString();
	char readBuffer[60000];
	memcpy(readBuffer, temp.c_str(), temp.size());
	rapidjson::Reader reader;
	rapidjson::FileReadStream is(stdin, readBuffer, sizeof(readBuffer));
	char writeBuffer[60000];
	rapidjson::FileWriteStream os(stdout, writeBuffer, sizeof(writeBuffer));
	rapidjson::PrettyWriter<rapidjson::FileWriteStream> prettyWriter(os);
	reader.Parse<rapidjson::kParseValidateEncodingFlag>(is, writer);

	while (!_kbhit());
}
